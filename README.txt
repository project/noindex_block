----------------------------------------------------------------------------------------------------------------
                                               Noindex Block
----------------------------------------------------------------------------------------------------------------


OVERVIEW
-----------------

The module allows to hide any block from being indexed by search engine (currently Yandex).
It wraps block content in the absolutely valid HTML code:
<!--noindex-->A block content<!--/noindex-->

Official Yandex documentation http://help.yandex.ru/webmaster/controlling-robot/html.xml#noindex

Sponsored by HookAny.com - Russian Drupal Job Board.


INSTALLATION
-----------------
Just enable the module and enjoy.


CONFIGURATION
-----------------
Got to block configuration page and enable 'Noindex block' checkbox then save settings.


AUTHOR
-----------------
Konstantin Komelin
https://drupal.org/user/1195752
